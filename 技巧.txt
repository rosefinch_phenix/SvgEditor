//取得图片文件的URL
function getImgFileUrl(sourceId) {
    var url;
    if (navigator.userAgent.indexOf("MSIE") >= 1) { // IE 
        url = document.getElementById(sourceId).value;
    } else if (navigator.userAgent.indexOf("Firefox") > 0) { // Firefox 
        url = window.URL.createObjectURL(document.getElementById(sourceId).files.item(0));
    } else if (navigator.userAgent.indexOf("Chrome") > 0) { // Chrome 
        url = window.URL.createObjectURL(document.getElementById(sourceId).files.item(0));
    }
    return url;
}
logoFileCmp.on('change', function preImg(e, t, option) {
                                        var url = getImgFileUrl(t.id);
                                        var imgPre = document.getElementById("logoPic");
                                        imgPre.src = url;
                                    }, this);